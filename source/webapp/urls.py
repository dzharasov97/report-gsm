from django.urls import path
from webapp.views.index import IndexView
from webapp.views.fuel_consumption import FuelConsumptionCreateView
from webapp.views.dispatcher import DispatcherIndexView
from webapp.views.dashboard import DashboardIndexView

urlpatterns = [
    path('', IndexView.as_view(), name='homepage'),
    path('fuel_consumption/create/<int:pk>', FuelConsumptionCreateView.as_view(), name='fuel_consumption_create'),
    path('fuel_consumption/', DispatcherIndexView.as_view(), name='fuel_consumption'),
    path('dashboards/', DashboardIndexView.as_view(), name='dashboards')
]
