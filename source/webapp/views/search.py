import json
from datetime import datetime
from django.core.serializers import serialize
from django.http import HttpResponse, HttpResponseNotAllowed, JsonResponse
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import ensure_csrf_cookie
from webapp.models import Cars


def json_cars(request, *args, **kwargs):
    if request.method == 'GET':
        search = request.GET.get('search')
        cars = Cars.objects.filter(government_number__icontains=search) if search else Cars.objects.all()
        return JsonResponse(list(cars.values(*('id', 'brand', 'model', 'government_number'))), safe=False)
    if request.method == 'POST' and request.body:
        car = json.loads(request.body)
        try:
            car = Cars.objects.create(**car)
            response = JsonResponse(car.as_dict)
            response.status_code = 201
        except Exception as e:
            response_data = {"detail": "Некорректный набор полей."}
            response = JsonResponse(response_data)
            response.status_code = 400
        return response

