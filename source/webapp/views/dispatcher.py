from django.views.generic import ListView
from django.db.models import Q
from django.utils.http import urlencode
from webapp.forms import SearchForm
from webapp.models import Cars, GsmCategories, Cities, FuelConsumption
from datetime import datetime
from django.contrib.auth.models import User


class DispatcherIndexView(ListView):
    template_name = 'dispatcher/dispatcher.html'
    context_object_name = 'fuel_consumption'
    model = FuelConsumption
    ordering = ('-refueling_date',)

    def get(self, request, *args, **kwargs):
        self.form = self.get_search_form()
        self.search_value = self.get_search_value()
        self.author = request.GET.get('author')
        self.start_date = request.GET.get('start_date')
        self.end_date = request.GET.get('end_date')
        return super().get(request, *args, **kwargs)

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(DispatcherIndexView, self).get_context_data(object_list=object_list, **kwargs)
        # fuel_consumption = FuelConsumption.objects.all().order_by('-refueling_date')
        # context['fuel_consumption'] = fuel_consumption
        context['gsm_categories'] = GsmCategories.objects.all()
        context['authors'] = User.objects.all()
        context['form'] = self.form
        context['dep'] = self.author
        context['government_number'] = self.search_value
        context['get_sum_diesel'] = FuelConsumption().get_sum_diesel
        context['get_sum_fuel'] = FuelConsumption().get_sum_fuel
        context['get_sum_gas'] = FuelConsumption().get_sum_gas
        if self.start_date or self.end_date:
            context['start_date'] = datetime.strptime(self.start_date, "%Y-%m-%d").date()
            context['end_date'] = datetime.strptime(self.end_date, "%Y-%m-%d").date()
        elif self.search_value:
            context['query'] = urlencode({'search': self.search_value})
        return context

    def get_queryset(self):
        queryset = super().get_queryset()
        car = list(map(lambda c: c.pk, Cars.objects.filter(government_number=self.search_value)))
        author = User.objects.filter(username=self.author)
        author_title = list(map(lambda a: a.pk, author))
        try:
            if self.author and self.search_value and self.start_date and self.end_date:
                query = Q(car=car[0])
                if self.author == "Все депы":
                    queryset = FuelConsumption.objects.filter(Q(refueling_date__gte=self.start_date) & Q(refueling_date__lte=self.end_date)) & queryset.filter(query)
                else:
                    queryset = FuelConsumption.objects.filter(Q(author=author_title[0]) & Q(refueling_date__gte=self.start_date) & Q(refueling_date__lte=self.end_date)) & queryset.filter(query)
            elif self.author and self.start_date and self.end_date:
                if self.author == "Все депы":
                    queryset =  FuelConsumption.objects.filter(Q(refueling_date__gte=self.start_date ) & Q(refueling_date__lte=self.end_date))
                else:
                    queryset = FuelConsumption.objects.filter(Q(author=author_title[0]) & Q(refueling_date__gte=self.start_date) & Q(refueling_date__lte=self.end_date))
            elif self.search_value:
                query = Q(car=car[0])
                queryset = queryset.filter(query)
            elif self.author:
                if self.author == "Все депы":
                    return queryset
                else:
                    queryset = FuelConsumption.objects.filter(Q(author=author_title[0]))
            elif self.search_value and self.author:
                query = Q(car=car[0])
                queryset = FuelConsumption.objects.filter(Q(author=author_title[0])) & queryset.filter(query)
            else:
                queryset = FuelConsumption.objects.all().order_by('car__gsm_category__title', 'brand')
        except BaseException:
            return queryset
        return queryset

    def get_search_form(self):
        return SearchForm(self.request.GET)

    def get_search_value(self):
        if self.form.is_valid():
            return self.form.cleaned_data['search']
        return None
