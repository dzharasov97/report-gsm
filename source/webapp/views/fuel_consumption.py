from django.shortcuts import redirect, get_object_or_404
from django.views.generic import FormView
from webapp.models import FuelConsumption, Cars
import datetime
from webapp.forms import FuelConsumptionForm


class FuelConsumptionCreateView(FormView):
    form_class = FuelConsumptionForm

    def post(self, request, *args, **kwargs):
        car = get_object_or_404(Cars, pk=kwargs.get('pk'))
        form = self.get_form_class()(request.POST)
        if form.is_valid():
            refueling_liters = form.cleaned_data.get('refueling_liters')
            refueling_date = datetime.date.today()
            author = request.user
            FuelConsumption.objects.create(car=car, author=author, refueling_date=refueling_date, refueling_liters=refueling_liters)
        return redirect('homepage')

