from wialon import Wialon, WialonError
import requests

def report(sid):
    url = f'https://hst-api.wialon.com/wialon/ajax.html?svc=core/search_items&params=' \
          f'{{"spec":{{"itemsType":"avl_unit","propName":"sys_name","propValueMask":"*","sortType":"sys_name"}},' \
          f'"force":1,"flags":1025,"from":0,"to":0}}&sid={sid}'
    return url

def request_wialon():
    try:
        # Создаем объект Wialon
        wialon_api = Wialon()

        # Выполняем вход через токен
        result = wialon_api.token_login(token='a4f84f67f7f6e9492fccf8131b7c2b5d64038E81CACB6D2858F88F770919D0937863ED26')

        # Получаем sid
        wialon_api.sid = result['eid']

        #Формируем запрос для полей online и offline
        url_report = report(wialon_api.sid)
        response_report = requests.get(url_report)
        data_report = response_report.json()
        print(data_report)

        # Выходим из аккаунта
        wialon_api.core_logout()

        #Обрабатываем и возвращаем данные
        return

    except WialonError or BaseException as e:
        # Обработка ошибок
        return  print(f"Ошибка: {e}")

