from datetime import datetime
from django.views.generic import TemplateView
from wialon import Wialon, WialonError
import requests


"http://hosting.wialon.com/login.html?access_type=-1&activation_time=2592000"

# Получаем данные по счетчикам и основные данные по объектам
def all_objects(sid):
    url = f'https://hst-api.wialon.com/wialon/ajax.html?svc=core/search_items&params=' \
           f'{{"spec":{{"itemsType":"avl_unit","propName":"sys_name","propValueMask":"*","sortType":"sys_name"}},"force":1,"flags":8193,"from":0,"to":0}}&sid={sid}'
    return url

# онлайн и оффлайн
def on_off(sid):
    url = f'https://hst-api.wialon.com/wialon/ajax.html?svc=core/search_items&params=' \
          f'{{"spec":{{"itemsType":"avl_unit","propName":"sys_name","propValueMask":"*","sortType":"sys_name"}},' \
          f'"force":1,"flags":1025,"from":0,"to":0}}&sid={sid}'
    return url

# сливы
def drain(sid):
    url = f'https://hst-api.wialon.com/wialon/ajax.html?svc=report/exec_report&params=' \
          f'{{"reportResourceId":25991247,"reportTemplateId":1,"reportTemplate":null,"reportObjectId":26013533,"reportObjectSecId":0,"interval":' \
          f'{{"flags":16777216,"from":1679335200,"to":1679443140}}}}&sid={sid}'
    return url

def count_on_off(data):
    lmsg_none = []
    count_on = 0
    count_off = 0
    for item in data['items']:
        if item['lmsg'] is None:
            count_off += 1
            lmsg_none.append(item)
        else:
            lmsg_rt = item['lmsg']['rt']
            dt_now = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            value = datetime.fromtimestamp(lmsg_rt)
            dt_position = value.strftime('%Y-%m-%d %H:%M:%S')
            dt_now_str = datetime.strptime(dt_now, '%Y-%m-%d %H:%M:%S')
            dt_position_str = datetime.strptime(dt_position, '%Y-%m-%d %H:%M:%S')
            difference = dt_now_str - dt_position_str
            days = difference.days
            seconds = difference.seconds
            hours = seconds // 3600
            if days > 0 and hours >= 3:
                count_off += 1
            else:
                count_on += 1
    return count_on, count_off, lmsg_none

def get_all_pk(data):
    pk_lst = []
    for item in data['items']:
        object_pk = f"Транспорт: {item['nm']} - id_виалон: {item['id']}"
        pk_lst.append(object_pk)
    return pk_lst


def api_wialon():
    try:
        # Создаем объект Wialon
        wialon_api = Wialon()

        # Выполняем вход через токен
        result = wialon_api.token_login(token='a4f84f67f7f6e9492fccf8131b7c2b5d64038E81CACB6D2858F88F770919D0937863ED26')

        # Получаем sid
        wialon_api.sid = result['eid']

        #Формируем запрос для полей online и offline
        url_on_off = on_off(wialon_api.sid)
        response_on_off = requests.get(url_on_off)
        data_on_off = response_on_off.json()

        # Формируем список со всеми id объектов
        url_all_objects = all_objects(wialon_api.sid)
        response_all_objects = requests.get(url_all_objects)
        data_all_objects = response_all_objects.json()
        all_pk = get_all_pk(data_all_objects)
        for a in all_pk:
            print(a)

        # url_drain = drain(wialon_api.sid, all_pk)
        # response_drain = requests.get(url_drain)
        # data_drain = response_drain.json()

        url_drain = drain(wialon_api.sid)
        response_drain = requests.get(url_drain)
        data_drain = response_drain.json()


        # Выходим из аккаунта
        wialon_api.core_logout()

        #Обрабатываем и возвращаем данные
        return count_on_off(data_on_off)

    except WialonError or BaseException as e:
        # Обработка ошибок
        return  print(f"Ошибка: {e}")


# def list_on_off():
#     data = api_wialon()
#     data_on_off = data[2]
#     for item in data_on_off:
#         return item['nm']


class DashboardIndexView(TemplateView):
    template_name = 'dashboard/dashboard.html'

    def get(self, request, *args, **kwargs):
        self.off = request.GET.get('off')
        return super().get(request, *args, **kwargs)

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(DashboardIndexView, self).get_context_data(object_list=object_list, **kwargs)
        wialon_data = api_wialon()
        context['online'] = wialon_data[0]
        context['offline'] = wialon_data[1]
        # context['drain'] = wialon_data[2]
        return context

    # def get_queryset(self):
    #     queryset = super().get_queryset()
    #     if self.off:
    #         queryset = list_on_off()
    #     return queryset
