from django.views.generic import ListView
from django.db.models import Q
from django.utils.http import urlencode
from webapp.forms import SearchForm
from webapp.models import Cars, GsmCategories, Cities, FuelConsumption
import datetime


class IndexView(ListView):
    template_name = 'index.html'
    context_object_name = 'cars'
    model = Cars

    def get(self, request, *args, **kwargs):
        self.form = self.get_search_form()
        self.search_value = self.get_search_value()
        return super().get(request, *args, **kwargs)

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(IndexView, self).get_context_data(object_list=object_list, **kwargs)
        today = datetime.datetime.now().date()
        # fuel_consumption = list(map(lambda f: f.car.pk, FuelConsumption.objects.all()))
        fuel_consumption = list(map(lambda f: f.car.pk, FuelConsumption.objects.filter(refueling_date=today)))
        context['today'] = today.strftime("%d-%m-%Y")
        context['fuel_consumption'] = fuel_consumption
        context['liters'] = FuelConsumption.objects.filter(refueling_date=today)
        context["search_title"] = self.search_value
        context['form'] = self.form
        if self.search_value:
            context['query'] = urlencode({'search': self.search_value})
        return context

    def get_queryset(self):
        queryset = super().get_queryset()
        if self.search_value:
            query = Q(government_number__icontains=self.search_value)
            queryset = queryset.filter(query)
        return queryset

    def get_search_form(self):
        return SearchForm(self.request.GET)

    def get_search_value(self):
        if self.form.is_valid():
            return self.form.cleaned_data['search']
        return None

