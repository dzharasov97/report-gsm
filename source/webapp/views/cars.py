from django.views.generic import DetailView
from webapp.models import Cars


class CarDetailView(DetailView):
    template_name = 'cars/car.html'
    model = Cars

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

