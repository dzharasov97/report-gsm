from django.contrib import admin
from webapp.models import CarCategories, GsmCategories, UtensilsCategories, Cars, Cities, FuelConsumption


admin.site.register(CarCategories)

admin.site.register(GsmCategories)

admin.site.register(UtensilsCategories)

admin.site.register(Cars)

admin.site.register(Cities)

admin.site.register(FuelConsumption)

