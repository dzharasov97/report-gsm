from django.db import models
from django.contrib.auth.models import User
from webapp.models import GsmCategories


class FuelConsumption(models.Model):
    car = models.ForeignKey(
        verbose_name='Транспорт',
        to='webapp.Cars',
        related_name='fuel_consumption',
        null=False,
        blank=False,
        on_delete=models.CASCADE
    )
    author = models.ForeignKey(
        to=User,
        on_delete=models.CASCADE,
        related_name='fuel_consumption',
        verbose_name='Дэп',
        blank=True
    )
    refueling_date = models.DateTimeField(
        verbose_name='Дата заправки',
        max_length=100,
        null=True,
        blank=False
    )
    refueling_liters = models.PositiveIntegerField(
        verbose_name='Кол-во литров',
        default=0,
        null=True,
        blank=False
    )
    is_deleted = models.BooleanField(
        verbose_name='Удален',
        default=False
    )
    created_at = models.DateTimeField(
        verbose_name='Дата создания',
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        verbose_name='Дата редактирования',
        auto_now=True
    )

    def get_sum_diesel(self):
        fuel_consumption = FuelConsumption.objects.filter(car__gsm_category__title="ДТ")
        refueling_liters_sum = list(map(lambda f: f.refueling_liters, fuel_consumption))
        return sum(refueling_liters_sum)

    def get_sum_fuel(self):
        fuel_consumption = FuelConsumption.objects.filter(car__gsm_category__title="АИ")
        refueling_liters_sum = list(map(lambda f: f.refueling_liters, fuel_consumption))
        return sum(refueling_liters_sum)

    def get_sum_gas(self):
        fuel_consumption = FuelConsumption.objects.filter(car__gsm_category__title="Газ")
        refueling_liters_sum = list(map(lambda f: f.refueling_liters, fuel_consumption))
        return sum(refueling_liters_sum)

    class Meta:
        verbose_name = 'Расход ГСМ'
        verbose_name_plural = 'Расходы ГСМ'

    def __str__(self):
        return f'{self.car}: {self.refueling_date} - {self.refueling_liters}'

