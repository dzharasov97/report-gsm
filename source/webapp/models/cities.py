from django.db import models


class Cities(models.Model):
    title = models.CharField(
        verbose_name='Название',
        null=False,
        blank=False,
        max_length=100
    )
    is_deleted = models.BooleanField(
        verbose_name='Удален',
        default=False
    )
    created_at = models.DateTimeField(
        verbose_name='Дата создания',
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        verbose_name='Дата редактирования',
        auto_now=True
    )

    class Meta:
        verbose_name = 'Город'
        verbose_name_plural = 'Города'

    def __str__(self):
        return self.title

