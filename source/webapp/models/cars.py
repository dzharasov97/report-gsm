from django.db import models
from django.core import validators
import datetime


class Cars(models.Model):
    brand = models.CharField(
        verbose_name='Марка',
        null=False,
        blank=False,
        max_length=100
    )
    model = models.CharField(
        verbose_name='Модель',
        null=False,
        blank=False,
        max_length=100
    )
    government_number = models.CharField(
        verbose_name='Государственный номер',
        null=False,
        blank=False,
        max_length=100
    )
    min_consumption = models.FloatField(
        verbose_name='Мин. расход литров',
        validators=[validators.MinValueValidator(0.0)],
        null=False,
        blank=False
    )
    max_consumption = models.FloatField(
        verbose_name='Макс. расход литров',
        validators=[validators.MinValueValidator(0.0)],
        null=False,
        blank=False
    )
    city = models.ForeignKey(
        to='webapp.Cities',
        verbose_name='Город',
        related_name='cars',
        on_delete=models.CASCADE,
        null=True,
        blank=False
    )
    utensils_category = models.ForeignKey(
        to='webapp.UtensilsCategories',
        verbose_name='Категория принадлежности',
        related_name='cars',
        on_delete=models.CASCADE,
        null=True,
        blank=False
    )
    car_category = models.ForeignKey(
        to='webapp.CarCategories',
        verbose_name='Категория транспорта',
        related_name='cars',
        on_delete=models.CASCADE,
        null=True,
        blank=False
    )
    gsm_category = models.ForeignKey(
        to='webapp.GsmCategories',
        verbose_name='Категория ГСМ',
        related_name='cars',
        on_delete=models.CASCADE,
        null=True,
        blank=False
    )
    is_deleted = models.BooleanField(
        verbose_name='Удален',
        default=False
    )
    created_at = models.DateTimeField(
        verbose_name='Дата создания',
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        verbose_name='Дата редактирования',
        auto_now=True
    )


    class Meta:
        verbose_name = 'Транспорт'
        verbose_name_plural = 'Транспорт'

    def __str__(self):
        return f"{self.brand} - {self.model} - {self.government_number}"

