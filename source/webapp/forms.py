from django import forms
from django.contrib.auth import get_user_model


# class SearchForm(forms.Form):
#     search = forms.CharField(max_length=100, required=False, label="Поиск",
#                              widget=forms.TextInput
#                                    (attrs={
#                                  "placeholder" : "Введите гос.номер"
#                                }))

class SearchForm(forms.Form):
    search = forms.CharField(max_length=100, required=False, label='Найти')

class FuelConsumptionForm(forms.Form):
    refueling_liters = forms.CharField(required=True, label='Литры')

