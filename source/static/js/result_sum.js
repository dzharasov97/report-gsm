const tables = document.querySelectorAll('.table')
for (let j = 0; j < tables.length; j++) {
    let sum = 0

    const tds = tables[j].querySelectorAll(".gsm_liters")

    for (let i = 0; i < tds.length; i++) {
        sum += parseInt(tds[i].getAttribute('data-liters'))
    }

    const newRow = tables[j].insertRow(-1); // добавляем новую строку в конец таблицы
    const newCell1 = newRow.insertCell(0); // добавляем первую ячейку
    const newCell2 = newRow.insertCell(-1); // добавляем вторую ячейку
    const newCell3 = newRow.insertCell(-1); // добавляем вторую ячейку
    const newCell4 = newRow.insertCell(-1); // добавляем вторую ячейку
    const newCell5 = newRow.insertCell(-1); // добавляем вторую ячейку
    newCell1.innerHTML = '&nbsp;'; // добавляем пустое содержимое во вторую ячейку
    newCell2.innerHTML = '&nbsp;'; // добавляем пустое содержимое во вторую ячейку
    newCell3.innerHTML = '&nbsp;'; // добавляем пустое содержимое во вторую ячейку
    newCell4.innerHTML = '&nbsp;'; // добавляем пустое содержимое во вторую ячейку
    newCell5.innerHTML = `Итог: ${sum}`; // добавляем в нее результат
}
