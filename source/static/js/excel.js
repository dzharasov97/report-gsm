function exportTableToExcel(filename = '') {
  var downloadLink;
  var dataType = 'application/vnd.ms-excel';
  var tableSelect = document.querySelectorAll('.table');
  var tableHTMLs = [];

  for (let j = 0; j < tableSelect.length; j++) {
    tableHTMLs.push(tableSelect[j].outerHTML.replace(/ /g, '%20'));
  }

  var tableHTML = tableHTMLs.join('');

  filename = filename ? filename + '.xls' : 'excel_data.xls';

  downloadLink = document.createElement('a');
  document.body.appendChild(downloadLink);

  if (navigator.msSaveOrOpenBlob) {
    var blob = new Blob(['ufeff', tableHTML], {
      type: dataType
    });
    navigator.msSaveOrOpenBlob(blob, filename);
  } else {
    downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
    downloadLink.download = filename;
    downloadLink.click();
  }
}