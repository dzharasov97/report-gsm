from django import forms


class LoginForm(forms.Form):
    username = forms.CharField(required=False, label='Логин',
                               widget=forms.TextInput
                                   (attrs={
                                   'class': 'Логин',
                                   'name': 'Логин',
                                   'required': 'True'
                               }))
    password = forms.CharField(required=False, label='Пароль',
                               widget=forms.PasswordInput
                                   (attrs={
                                   'class': 'Пароль',
                                   'name': 'Пароль',
                                   'required': 'True'
                               }))
    next = forms.CharField(required=False, widget=forms.HiddenInput)

